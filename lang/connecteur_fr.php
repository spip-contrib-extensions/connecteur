<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'connecteur_titre' => 'Connecteur universel',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	'connexion_facebook' => 'Se connecter avec Facebook',
	// T
	'titre_page_configurer_connecteur' => 'Configurer les connexions',
);
